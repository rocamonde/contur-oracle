from typing import Tuple

import numpy as np


def rescale_to_range(array: np.ndarray, range: Tuple[int, int] = None):
    """
    Rescales an array to a given range, or to (0,1) if no range is provided.
    :param array: array to rescale
    :param range: range to rescale the array to. Defaults to (0, 1)
    :return: rescaled array
    """
    target_min, target_max = range or (0, 1)
    current_min, current_max = array.min(), array.max()

    target_span = target_max - target_min
    current_span = current_max - current_min
    return (array - current_min) * target_span / current_span + target_min
