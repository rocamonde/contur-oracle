import tensorflow as tf
from tensorflow import keras


def get_model() -> keras.Sequential:
    model = keras.Sequential([
        keras.layers.UpSampling2D(size=(10, 10), interpolation='bilinear'),
        keras.layers.Flatten(input_shape=(20,20)),
        keras.layers.Dense(32, activation=tf.nn.relu),
        keras.layers.Dense(32, activation=tf.nn.relu),
        keras.layers.Dense(32, activation=tf.nn.relu),
        keras.layers.Dense(32, activation=tf.nn.relu),
        keras.layers.Dense(32, activation=tf.nn.relu),
        keras.layers.Dense(1, activation=tf.nn.sigmoid),
    ])

    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model