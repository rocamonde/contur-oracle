from typing import Callable, Tuple, List

import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split
from tensorflow import keras

from utils import rescale_to_range

# The number of BSM params that the input layer of the NN hosts
NUM_BSM_PARAMS = 2
# We add a parameter for the normalised CL value at index 0
TOT_PARAMS = NUM_BSM_PARAMS + 1
# The number of BSM grid points from the initial Contur scan to be able to predict a
# point.
NUM_BSM_GRID_POINTS = 100
# We add another slot to indicate, as part of the input array for the Neural Network,
# the BSM point we want to predict.
TOT_BSM_GRID_POINTS = NUM_BSM_GRID_POINTS + 1
CL_THRESHOLD = 0.5
DEFAULT_EPOCHS = 50

testing_functions = [
    lambda x, y: np.sin(x + y) ** 2,
    lambda x, y: np.tanh(x ** 2 + y ** 2),
    lambda x, y: np.exp(- (x ** 2 + y ** 2)),
]


def get_model() -> keras.Sequential:
    """
    Initialises the Neural Network model blueprint, compiled but with no training data
    :return: NN keras sequential model, with a (TOT_PARAMS, NUM_BSM_GRID_POINTS) input
    layer and a sigmoid output layer.
    """
    model = keras.Sequential([
        keras.layers.Flatten(input_shape=(TOT_PARAMS, TOT_BSM_GRID_POINTS)),
        keras.layers.Dense(16, activation=tf.nn.relu),
        keras.layers.Dense(16, activation=tf.nn.relu),
        keras.layers.Dense(16, activation=tf.nn.relu),
        keras.layers.Dense(16, activation=tf.nn.relu),
        keras.layers.Dense(1, activation=tf.nn.sigmoid),
    ])
    model.compile(optimizer='adam',
                  loss='binary_crossentropy',
                  metrics=['accuracy'])
    return model


def generalised_function():
    a, b = np.random.uniform(low=0.5, high=1, size=(2,))
    p1, p2 = np.random.rand(2) * 2 * np.pi

    def training_function(x, y):
        return np.sin(a * x + p1) + np.sin(b * y + p2)

    return training_function


def normalise_cl(arr: np.ndarray) -> np.ndarray:
    """
    Converts the raw CL value to something understandable by the neural network.
    Currently, its just a binary value of 0 or 1 indicating whether the BSM point is
    excluded.
    :param arr: array of arbitrary shape containing CL values
    :return: array of the same shape containing 0s or 1s depending on whether the CL
    value is above or below the CL threshold.
    """
    return (arr > CL_THRESHOLD).astype(int)


def get_training_points(
        num_function_gens,
        num_points_per_function
) -> (np.ndarray, np.ndarray):
    """
    Generates training points using the generalised function
    :param num_function_gens: Number of different functions to generate from the
    generalised function generator.
    :param num_points_per_function: number of different training points to create per
    each generated function. We want this to be higher than the number of BSM grid
    points so that the network can learn the behaviour of that specific function well
    enough.
    :return: A tuple containing the training input and output arrays for the neural
    network training.
    The training input array has shape (num_function_gens * num_points_per_function,
    TOT_PARAMS, TOT_BSM_GRID_POINTS) and has the following axes:
     - 0th axis: each of the training points to feed into the NN. Every training point
     contains the initial BSM grid scan and the desired BSM point to predict.
     - 1st axis: each of the BSM parameters. The first index is always the normalised
     CL value, while . The rest of the indices are each of the BSM model parameters.
     - 2nd axis: each of the BSM grid points that have been generated. The first index
    is always the desired point to predict, whose CL value is replaced by padding,
    and the other indices are the result of the Contur scan or, during training,
    of scanning a random function.
    The training output array is a vector with shape (num_function_gens *
    num_points_per_function, ) and each point of the vector is a binary 0 or 1 value
    that labels whether each of the BSM points to predict are excluded.
    """
    # The number of total training points to generate
    num_points = num_function_gens * num_points_per_function

    # The input and output training arrays
    training_input = np.random.rand(num_points, TOT_PARAMS, TOT_BSM_GRID_POINTS)
    training_output = np.zeros((num_points,))

    for i in range(num_function_gens):
        # We generate a different function for each function generation iteration.
        training_function = generalised_function()

        # Training points slice for this function starts at
        tps = i * num_points_per_function
        # Training points slice for this function ends at
        tpe = (i + 1) * num_points_per_function

        # We generate the BSM grid as a binary value of 0: not excluded, 1: excluded.
        # The zeroth index of the second axis contains the BSM grid point exclusion label.
        # With this, we are also calculating the labels for the BSM point to predict.
        bsm_param_vals = training_input[tps:tpe].transpose((1, 0, 2))[1:TOT_PARAMS]
        training_input[tps:tpe, 0, :] = normalise_cl(training_function(*bsm_param_vals))

        # The values at [:, 0, 0] hold the labels for the target BSM points to predict,
        # and we want to pass them separately so they can be provided as the ML model
        # output for training.
        training_output[tps:tpe] = np.copy(training_input[tps:tpe, 0, 0])

        # Of course, we want to remove these values from the input data, so that the
        # network can learn to predict them. Since the array size is fixed, we use a
        # placeholder of 0.5.
        training_input[tps:tpe, 0, 0] = 0.5

    return training_input, training_output


def get_evaluation_points(
        bsm_initial_scan: np.ndarray,
        bsm_prediction_points: np.ndarray,
        *,
        rescale_bsm_params=True,
        normalise_cl_values=True,
) -> np.ndarray:
    """
    This function creates the input dataset to use for training or prediction of the
    NN model. It rescales BSM params to the (0, 1) interval and normalises the CL value
    according to the `normalise_cl()` function.

    :param bsm_initial_scan: This is the BSM model points obtained from the initial
    low-resolution Contur scan of the model, or a random array for testing and training,
    which has shape (TOT_PARAMS, NUM_BSM_GRID_POINTS). The BSM parameter values on (1:, :)
    are re-sampled from 0 to 1 and the CL value on (0, :) is normalised according to the
    `normalise_cl()` function.

    :param bsm_prediction_points: This is the BSM points that we want to run a
    prediction for, conditional to the `bsm_initial_scan` parameter. It expects an
    array of shape (num_prediction_points, NUM_BSM_PARAMS), where num_prediction_points is
    any arbitrary size. The first axis corresponds to each point in param space to
    predict for, while the second axis corresponds to each BSM param raw value (i.e.
    not rescaled). The CL values on the NN input array that are required due to shape
    constraints are automatically padded to the default value of 0.5.

    :param rescale_bsm_params: Optional keyword argument that allows overriding the
    BSM param resampling if this has already been done by another function to reduce
    computational overhead.

    :param normalise_cl_values: Optional keyword argument that allows overriding the
    CL value normalisation if this has already been done by another function to reduce
    computational overhead.

    :return: an array of shape (num_prediction_points, TOT_PARAMS, TOT_BSM_GRID_POINTS)
    that can be fed into the neural network.
    """

    assert bsm_initial_scan.shape[0] == TOT_PARAMS
    assert bsm_initial_scan.shape[1] == NUM_BSM_GRID_POINTS
    assert bsm_prediction_points.shape[1] == NUM_BSM_PARAMS

    n_points = bsm_prediction_points.shape[0]

    if rescale_bsm_params:
        bsm_prediction_points = rescale_to_range(bsm_prediction_points)
        bsm_initial_scan[1:, :] = rescale_to_range(bsm_initial_scan[1:, :])

    if normalise_cl_values:
        bsm_initial_scan[0, :] = normalise_cl(bsm_initial_scan[0, :])

    data_input = np.zeros((n_points, TOT_PARAMS, TOT_BSM_GRID_POINTS))
    data_input[:, :, 1:] = bsm_initial_scan
    data_input[:, 1:, 0] = bsm_prediction_points
    data_input[:, 0, 0] = 0.5

    return data_input


def get_validation_points(
        n_points: int,
        testing_function: Callable
) -> Tuple[np.ndarray, np.ndarray]:
    """
    Generates a (n_points, TOT_PARAMS, TOT_BSM_GRID_POINTS) array where for each
    validation point at axis 0 there is an input array to feed to the neural network
    for predicting or validating the BSM point on position (validation_point_idx,
    0, 0)
    :param n_points:
    :param testing_function:
    :return:
    """
    bsm_initial_scan = np.random.rand(TOT_PARAMS, NUM_BSM_GRID_POINTS)
    bsm_initial_scan[0, :] = testing_function(*bsm_initial_scan[1:TOT_PARAMS])

    bsm_prediction_points = np.random.rand(n_points, NUM_BSM_PARAMS)
    expected_result = testing_function(*bsm_prediction_points.transpose((1, 0)))
    input_data = get_evaluation_points(
        bsm_initial_scan=bsm_initial_scan,
        bsm_prediction_points=bsm_prediction_points,
        rescale_bsm_params=False
    )

    return input_data, expected_result


def train_model(
        model: keras.Sequential,
        *,
        n_func_gens: int,
        n_batches_per_func: int,
        batch_size: int,
        epochs: int = None,
        epoch_test_size: float = None,
        final_test_size: float = None,
        save_to: str = None,
) -> None:
    """
    Trains a given model using the get_training_points function.

    The total size of the training dataset is n_func_gens * n_batches_per_func *
    batch_size.

    The fraction of the dataset actually used for
    training is (1-epoch_test_size)*(1-final_test_size).

    :param model: model instance to train on
    :param n_func_gens: number of different abstract functions to create for training
    :param n_batches_per_func: number of batches to generate for each
    different function. This value times batch_size should be x5-x10 larger than the grid
    size so that the network can be trained properly.
    :param batch_size: the size of the training batches, as understood in the keras docs.
    :param epochs: the number of epochs, as understood in the keras docs.
    :param epoch_test_size: fraction of the training dataset to dedicate to testing on
    each epoch. If None, 0.15 (15%) is used
    :param final_test_size: fraction of the training dataset to dedicate to testing
    after the entire training process. If none, 0.15 (15%) is used.
    :param save_to: path to save the model to. If none is specified, it defaults to
    'model.tfmodel', relative to the current working directory.
    """
    x, y = get_training_points(n_func_gens, n_batches_per_func * batch_size)

    x_train, x_test, y_train, y_test = train_test_split(
        x, y,
        test_size=final_test_size or 0.15,
        random_state=0)

    model.fit(
        x_train, y_train,
        epochs=epochs or DEFAULT_EPOCHS,
        batch_size=batch_size,
        validation_split=epoch_test_size or 0.15)

    test_loss, test_acc = model.evaluate(x_test, y_test)
    print('Test accuracy:', test_acc, '; test loss:', test_loss)
    model.save(save_to or 'model.tfmodel')


def load_model(file_path=None) -> keras.Sequential:
    """
    Loads a trained model from local storage
    :param file_path: optional path to model, default is None and will load the
    "model.tfmodel" file.
    :return: a keras Sequential model like in get_model() but (presumably) already
    trained.
    """
    return keras.models.load_model(file_path or "model.tfmodel")


def plot_model_performance(model: keras.Sequential):
    input_data, expected_result = get_validation_points(2000, testing_functions[0])

    result = model.predict(input_data, batch_size=1)

    figure, axes_l = plt.subplots(ncols=3)
    figure: plt.Figure
    axes_l: List[plt.Axes]

    axes_l[0].scatter(
        input_data[:, 1, 0],
        input_data[:, 2, 0],
        s=20,
        c=result,
        cmap=plt.cm.coolwarm
    )

    axes_l[1].scatter(
        input_data[1, :],
        input_data[2, :],
        s=20,
        c=input_data[0, :],
        cmap=plt.cm.coolwarm
    )

    axes_l[2].scatter(
        input_data[1, :],
        input_data[2, :],
        s=20,
        c=input_data[0, :],
        cmap=plt.cm.coolwarm
    )
    plt.show()
