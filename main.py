import sys

from model import *


def train_model_loop(new=False):
    model = load_model() if not new else get_model()
    while True:
        train_model(model,
                    n_func_gens=20,
                    n_batches_per_func=1000,
                    batch_size=128,
                    epochs=20,
                    epoch_test_size=0.15,
                    final_test_size=0.15
                    )


if __name__ == '__main__':
    command = sys.argv[1]
    if command == 'train':
        train_model_loop()
    if command == 'train-fresh':
        train_model_loop(new=True)
    if command == 'plot':
        model = load_model()
        plot_model_performance(model)

# TODO What happens when the resolution is not high enough and we confuse the network?
#  (think of stuff like RegionPlot[Sin[x + y]^2 > 0.5, {x, -10, 10}, {y, -10, 10},
#  PlotPoints -> 100])
